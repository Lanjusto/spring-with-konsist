package spring.with.konsist

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class ControllerTest{

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    fun simpleTest() {
        mockMvc.post("/v3/accounts") {
            contentType = MediaType.APPLICATION_JSON
            content = "Hey!"
            accept = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isCreated() }
        }
    }
}
