package spring.with.konsist

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(
    path = ["v3/accounts"],
    produces = ["application/json"]
)
internal class Controller{

    @PostMapping
    fun simpleEndpoint(
        @RequestBody
        payload: String
    ): ResponseEntity<String> {
        return ResponseEntity("$payload, CREATED", HttpStatus.CREATED)
    }
}
