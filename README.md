# Spring with Konsist

The purpose if this repository is to illustrate an issue with Konsist in Spring projects.

If pom.xml specifies `org.springframework.boot:spring-boot-starter-parent` as a parent (as suggested 
[here](https://docs.spring.io/spring-boot/docs/3.1.3/maven-plugin/reference/htmlsingle/#using.parent-pom)), then adding 
Konsist dependency makes Spring tests failing.

## Prerequisites

Have Maven installed. Tested on 3.9.2 and 3.9.3.

## Steps to Reproduce

1. Build the project: `mvn clean package`.
2. Make sure it has been built successfully and the single test has passed successfully.
3. Uncomment Konsist dependency in the pom.xml file (lines 42—47).
4. Run `mvn clean package` again.

## Expected Behaviour

Project has been built successfully.

## Actual Behaviour

The test is failed with the following error:

`ControllerTest.simpleTest » IllegalState Failed to load ApplicationContext for [WebMergedContextConfiguration@39ad12b6 testClass = spring.with.konsist.ControllerTest, locations = [], classes = [spring.with.konsist.App], contextInitializerClasses = [], activeProfiles = [], propertySourceLocations = [], propertySourceProperties = ["org.springframework.boot.test.context.SpringBootTestContextBootstrapper=true", "server.port=0"], contextCustomizers = [[ImportsContextCustomizer@4eb45fec key = [org.springframework.boot.test.autoconfigure.web.servlet.MockMvcAutoConfiguration, org.springframework.boot.test.autoconfigure.web.servlet.MockMvcWebClientAutoConfiguration, org.springframework.boot.test.autoconfigure.web.servlet.MockMvcWebDriverAutoConfiguration, org.springframework.boot.autoconfigure.security.oauth2.client.servlet.OAuth2ClientAutoConfiguration, org.springframework.boot.autoconfigure.security.oauth2.resource.servlet.OAuth2ResourceServerAutoConfiguration, org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration, org.springframework.boot.autoconfigure.security.servlet.SecurityFilterAutoConfiguration, org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration, org.springframework.boot.test.autoconfigure.web.servlet.MockMvcSecurityConfiguration, org.springframework.boot.test.autoconfigure.web.reactive.WebTestClientAutoConfiguration]], org.springframework.boot.test.context.filter.ExcludeFilterContextCustomizer@6d60fe40, org.springframework.boot.test.json.DuplicateJsonObjectContextCustomizerFactory$DuplicateJsonObjectContextCustomizer@514646ef, org.springframework.boot.test.mock.mockito.MockitoContextCustomizer@0, org.springframework.boot.test.web.client.TestRestTemplateContextCustomizer@70cf32e3, org.springframework.boot.test.autoconfigure.actuate.observability.ObservabilityContextCustomizerFactory$DisableObservabilityContextCustomizer@1f, org.springframework.boot.test.autoconfigure.properties.PropertyMappingContextCustomizer@4b3fa0b3, org.springframework.boot.test.autoconfigure.web.servlet.WebDriverContextCustomizerFactory$Customizer@55cb6996, org.springframework.boot.test.context.SpringBootTestAnnotation@c72b7e72], resourceBasePath = "src/main/webapp", contextLoader = org.springframework.boot.test.context.SpringBootContextLoader, parent = null]`
